# Formulaire QFIELD de saisie de collisions

## Explications

Afin de faciliter le travail des patrouilleurs qui ne sont pas équipés d'outil de collecte des données de collisions, le Cerema propose un fomulaire [QFIELD](https://qfield.org/docs/fr/) et un pas à pas pour l'installation du formulaire et son utilisation.

[QFIELD](https://qfield.org/docs/fr/install/index.html) est une application pour ordiphone et tablette disponible sous Android et iOS qui permet la collecte et l'affichage de données SIG sur le terrain.

## Matériel nécessaire
- Ordiphone ou Tablette Android & l’application QField (dernière version)
- Tablette Apple & l’application QField (version Beta)
- Fichier Formulaire_QFIELD_collisions.7z fourni
1.  **Formulaire_QFIELD_collisions_AuRA.zip** pour une collecte sur Auvergne-Rhône-Alpes (réseau routier)
2. **Formulaire_QFIELD_collisions_BFC.zip** pour une collecte sur Bourgogne-Franche-Comté (réseau routier)

## Principes
1. Préparation de la collecte :
- Installation de QFIELD sur la table/ordiphone,
- Récupération du projet QGIS (répertoire Formulaire_QFIELD_collisions et tous les fichiers qu'il contient (table de saisie / fonds de plan / tables de repérage dont les routes)),
- Copie de l’ensemble du projet QGIS dans le répertoire ad'hoc de la tablette.

2. Collecte sur le terrain :
- Ouverture du projet dans QField,
- saisie de la localisation des points de collisions et des circonstances de la collision avec la tablette : tout est collecté dans une seule table : collisions_faune_route.shp.

3. Récupération des données issues de la collecte :
- Copie manuelle du répertoire de la tablette/ordiphone au répertoire de son PC.
- Travail directement avec les données depuis QGIS.
